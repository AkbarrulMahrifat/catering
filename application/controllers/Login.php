<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/23/18
 * Time: 10:39 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('M_login');
    }

    public function index(){
        $this->load->view('login');
    }

    /**
     *
     */
    public function login(){
        $u = $this->input->post('username');
        $p = $this->input->post('password');

        $a = $this->M_login->login($u,$p);
        if($a){
            if($this->session->userdata('level')==1) {
                redirect('Admin');
            }
            else{
                redirect('User/index');
            }
        }else{
            $this->session->set_flashdata('error','Username/password salah');
        }
    }

    public function logout(){
        session_destroy();
        redirect('/');
    }

    public function register(){
        $this->load->view('header.php');
        $this->load->view('register');
        $this->load->view('footer.php');
    }

    public function registeruser(){
        $username   =   $this->input->post('username');
        $password   =   $this->input->post('password');
        $nama       =   $this->input->post('nama');
        $alamat     =   $this->input->post('alamat');
        $jenis      =   $this->input->post('jenis_kelamin');
        $no_hp      =   $this->input->post('no_hp');

        $datauser = array(
            'username'  => $username,
            'password'  => md5($password),
            'level'     => 2
        );

        $datapelanggan = array(
            'id_pelanggan'  => null,
            'id_user'       => $username,
            'nama'          => $nama,
            'alamat'        => $alamat,
            'jenis_kelamin' => $jenis,
            'no_hp'         => $no_hp
        );

        $this->M_login->register($datauser,$datapelanggan);
        $this->session->set_flashdata('sukses','Register berhasil');
        redirect('Login');
    }

}
