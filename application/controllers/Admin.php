<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/1/18
 * Time: 11:34 AM
 */
class Admin extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->model('M_user');
    }

    function index(){
        if($this->session->userdata('level')!=='1'){
            redirect('/');
        }else {
            $this->load->view('Admin/header.php');
            $this->load->view('Admin/index.php');
            $this->load->view('Admin/footer.php');
        }
    }

    function menubarang(){
        $data['produk'] = $this->M_user->pilihproduk()->result();
        $this->load->view('Admin/header.php');
        $this->load->view('Admin/crudbarang.php',$data);
        $this->load->view('Admin/footer.php');
    }

    function menupesanan(){
        $data['pesanan'] = $this->M_admin->getdatapesanan();
        $this->load->view('Admin/header.php');
        $this->load->view('Admin/validasi.php',$data);
        $this->load->view('Admin/footer.php');
    }

    function validasi($id_pesanan){
        $set=array(
            'status' => 'Sudah diproses'
        );
        $this->M_admin->validasi($id_pesanan,$set);
        redirect('Admin/menupesanan');
    }

    function lihatnota($id_pesanan){
        $where = array('id_pesanan' => $id_pesanan);
        $data['pesanan'] = $this->M_user->pesanan($where, 'pesanan')->result();
        $this->load->view('Admin/header');
        $this->load->view('Admin/tambahnota', $data);
        $this->load->view('Admin/footer');
    }


    function showeditbarang($id){
        $data['produk'] = $this->M_admin->tampileditbarang($id);
        $this->load->view('Admin/header.php');
        $this->load->view('Admin/editbarang',$data);
        $this->load->view('Admin/footer.php');
    }

    function editbarang(){
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $harga = $this->input->post('harga');
        $stok = $this->input->post('stok');
        $satuan = $this->input->post('satuan');
        $keterangan = $this->input->post('keterangan');

        $editan = array(
            'nama_produk' => $nama,
            'harga' => $harga,
            'stok' => $stok,
            'satuan' => $satuan,
            'keterangan' => $keterangan
        );

        $this->M_admin->editbarang($id,$editan);
        redirect('Admin/menubarang');
    }



}
?>