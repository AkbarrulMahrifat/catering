<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?=base_url('assets/vendors/img/logoQ.png')?>" type="image/jpg" />

    <title>Al Qodiri</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="<?=base_url();?>/assets/vendors/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/vendors/css/font-awesome.css">
    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="<?=base_url();?>/assets/vendors/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/vendors/css/responsive.css">

    <script type="text/javascript" src="<?=base_url();?>/assets/vendors/js/modernizr.custom.js"></script>

    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
</head>

<body>
<div id="tf-home">
    <div class="container" style="min-height: 560px; padding-top: 80px; padding-bottom: 95px">
        <div class="row" align="center">
            <img src="<?=base_url();?>assets/vendors/img/logo.png" style="width: 500px">
            <?php if($this->session->flashdata('sukses')){echo '<div class="text-center"><h4>'.$this->session->flashdata('sukses').'</h4></div>';}?>
            <div style="border: 0; padding: 20px; background: none; width: 400px; text-align: center; color: #000000;" >
                <form action="<?=base_url('Login/login')?>" method="post">
                    <div>
                        <h3 align="left">Username</h3>
                        <p style="color:red";>
                            <?php if($this->session->flashdata('error')){
                                echo $this->session->flashdata('error');
                            }?>
                        </p>
                        <input type="text" class="form-control" placeholder="Username" name="username" required="" />
                    </div>
                    <div>
                        <h3 align="left">Password</h3>
                        <input type="password" class="form-control" placeholder="Password" name="password" required="" />
                    </div>
                    <br>
                    <div class="text-center">
                        <button type="submit" name='submit' class="btn btn-primary my-btn" value="Login" >Login</button>
                        <a href="<?=base_url('Login/register')?>" class="btn btn-primary my-btn">Register</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

  <!-- jQuery -->
  <script src="<?=base_url();?>/assets/vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?=base_url();?>/assets/vendors/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="<?=base_url();?>/assets/vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="<?=base_url();?>/assets/vendors/nprogress/nprogress.js"></script>

  <!-- Custom Theme Scripts -->
  <script src="<?=base_url();?>/assets/vendors/js/custom.min.js"></script>

</body>
</html>
