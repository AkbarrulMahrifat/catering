<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/18
 * Time: 11:15 PM
 */
?>

<div id="tf-contact">
    <div class="container">
        <div class="section-title" style="color: #111111; text-align: center;">
            <h2>Halaman Pembuatan Akun</h2>
        </div>

        <div class="space"></div>

        <div class="row" align="center">
            <form method="post" action="<?php echo base_url().'Login/registeruser';?>">
                <table>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Username</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" name="username" required>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Password</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="password" class="form-group" name="password" required>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Nama</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" name="nama" required>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Alamat</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" name="alamat" required>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Jenis Kelamin</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <select type="text" class="form-group" name="jenis_kelamin" required>
                                <optgroup label="Pilih Jenis Kelamin">
                                    <option value="Laki-laki">L</option>
                                    <option value="Perempuan">P</option>
                                </optgroup>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>No. HP</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" name="no_hp" required>
                        </td>
                    </tr>
                </table>
                <button type="submit" class="btn btn-primary my-btn">Simpan</button>
                <a href="" class="btn btn-primary my-btn">Batal</a>
            </form>
        </div>

    </div>
</div>