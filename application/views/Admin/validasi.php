<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/23/18
 * Time: 11:53 PM
 */
?>

<div id="tf-contact">
    <div class="container">
        <div class="section-title" style="color: #111111; text-align: center;">
            <h2>Halaman Validasi Pesanan</h2>
        </div>
        <table id="validasi" class="display" style="width:100%">
            <thead>
            <tr>
                <th>ID Pesanan</th>
                <th>Tanggal Pesanan</th>
                <th>Total</th>
                <th>Bukti Bayar</th>
                <th>Status</th>
                <th>ID Pelanggan</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($pesanan as $a){?>
                    <tr>
                        <td class="text-center"><?=$a['id_pesanan']?></td>
                        <td class="text-center"><?=$a['tanggal']?></td>
                        <td class="text-center"><?=$a['total']?></td>
                        <td class="text-center">
                            <?php if (!empty($a['bukti_bayar'])){ ?>
                                <a class="btn btn-sm btn-success" href="<?=site_url('Admin/lihatnota/'.$a['id_pesanan']) ?>"><li class="fa fa-check"> Lihat Nota</li></a>
                            <?php } else{ ?>
                                <a class="btn btn-sm btn-warning" disabled><li class="fa fa-times"> Nota belum ada</li></a>
                            <?php } ?>
                        </td>
                        <td class="text-center"><?=$a['status']?></td>
                        <td class="text-center"><?=$a['id_pelanggan']?></td>
                        <td class="text-center"><?php if($a['status'] == 'Belum diproses'){?><a data-confirm="Apakah anda yakin mau validasi pesanan ini?" href="<?=base_url()?>Admin/validasi/<?=$a['id_pesanan']?>" class="validasi btn btn-sm btn-primary">Validasi</a><?php }else{?><button class="btn btn-sm btn-success">Telah divalidasi</button><?php }?></td>
                    </tr>
                <?php }?>
            </tbody>
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--                <td>Tes</td>-->
<!--            </tr>-->
        </table>
    </div>
</div>


