<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/18
 * Time: 11:15 PM
 */
?>

<div id="tf-contact">
    <div class="container">
        <div class="section-title" style="color: #111111; text-align: center;">
            <h2>Halaman Tambah Stok</h2>
        </div>

        <div class="space"></div>

        <div class="row" align="center">
            <table>
                <tr>
                    <?php foreach ($produk as $p) { ?>
                    <td style="width: 10px;"></td>
                    <td>
                        <div style="padding: 20px; background: url(<?=base_url()?>assets/vendors/img/transparan.png); color: #FFFFFF;">
                            <table>
                                <tr>
                                    <td width=300px; align="center">
                                        <img src="<?=base_url()?>assets/vendors/img/<?php echo $p->gambar ?>" style="width: 150px">
                                        <h3><?php echo $p->nama_produk ?></h3>
                                        <p><?php echo $p->keterangan ?></p>
                                        <p>Harga <?php echo "Rp. ".number_format($p->harga,2,',','.'); ?> per <?php echo $p->satuan ?>.</p>
                                        <p>Sisa Stok : <?php echo $p->stok ?> <?php echo $p->satuan ?>.</p>
                                        <a href="<?=base_url()?>Admin/showeditbarang/<?=$p->id_produk?>" class="btn btn-primary">Edit</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td style="width: 10px;"></td>
                    <?php } ?>
                </tr>
            </table>


        </div>

    </div>
</div>