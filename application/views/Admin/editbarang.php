<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/17/18
 * Time: 11:15 PM
 */
?>

<div id="tf-contact">
    <div class="container">
        <div class="section-title" style="color: #111111; text-align: center;">
            <h2>Halaman Edit Barang</h2>
        </div>

        <div class="space"></div>

        <div class="row" align="center">
            <form method="post" action="<?php echo base_url().'Admin/editbarang';?>">

                <input type="hidden" value="<?php echo $produk->id_produk; ?>" name="id">
                <table>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Nama Produk</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" value="<?php echo $produk->nama_produk; ?>" name="nama" required>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Harga Produk</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" value="<?php echo $produk->harga; ?>" name="harga" required>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Stok Produk</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" value="<?php echo $produk->stok; ?>" name="stok" required>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Satuan</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" value="<?php echo $produk->satuan; ?>" name="satuan" required>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><label>Keterangan</label></td>
                        <td width="5%" style="vertical-align: top; text-align: center;"><label>:</label></td>
                        <td>
                            <input type="text" class="form-group" value="<?php echo $produk->keterangan; ?>" name="keterangan" required>
                        </td>
                    </tr>
                </table>
                <button type="submit" class="btn btn-primary my-btn">Simpan</button>
                <a href="" class="btn btn-primary my-btn">Batal</a>
            </form>
        </div>

    </div>
</div>