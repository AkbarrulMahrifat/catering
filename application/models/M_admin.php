<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 4/27/18
 * Time: 8:24 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model{

    function getdatapesanan(){
        $this->db->select('*');
        $this->db->from('pesanan');
        return $this->db->get()->result_array();
    }

    function validasi($id,$data){
        $this->db->where('id_pesanan',$id);
        $this->db->update('pesanan',$data);
    }

    function tampileditbarang($id){
        $this->db->where('id_produk',$id);
        return $this->db->get('produk')->row();
    }

    function editbarang($id,$data){
        $this->db->where('id_produk',$id);
        $this->db->update('produk',$data);
    }

}